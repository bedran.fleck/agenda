
import java.io.*;

public class CadastroPessoa {

    public static void main(String[] args) throws IOException {
        //burocracia para leitura de teclado
        InputStream entradaSistema = System.in;
        InputStreamReader leitor = new InputStreamReader(entradaSistema);
        BufferedReader leitorEntrada = new BufferedReader(leitor);
        String entradaTeclado;

        //instanciando objetos do sistema
        ControlePessoa umControle = new ControlePessoa();
        Pessoa umaPessoa = new Pessoa();

        double opcao = 5.0;

        do {
            System.out.println("O que deseja fazer? \n1) Adicionar pessoa. \n2) Pesquisar pessoa. \n3) Remover pessoa.\n0) Sair do Programa.");
            System.out.print("Sua Opção: ");
            entradaTeclado = leitorEntrada.readLine();
            String numeroOpcao = entradaTeclado;
            opcao = Double.parseDouble(numeroOpcao);

            if (opcao == 1.0) {
                System.out.println("Digite o nome da Pessoa:");
                entradaTeclado = leitorEntrada.readLine();
                String umNome = entradaTeclado;
                umaPessoa.setNome(umNome);

                System.out.println("Digite o telefone da Pessoa:");
                entradaTeclado = leitorEntrada.readLine();
                String umTelefone = entradaTeclado;
                umaPessoa.setTelefone(umTelefone);

                String mensagem = umControle.adicionar(umaPessoa);

                System.out.println("=================================");
                System.out.println(mensagem);

                System.out.println("Deseja adicionar mais informacoes do cadastro?\n 1 = sim \n 2 = nao");
                entradaTeclado = leitorEntrada.readLine();
                String opcaoCadastroString = entradaTeclado;
                double opcaoCadastro = Double.parseDouble(opcaoCadastroString);



                if (opcaoCadastro == 1.0) {
                    System.out.println("Digite a Idade da pessoa:");
                    entradaTeclado = leitorEntrada.readLine();
                    String umaIdade = entradaTeclado;
                    umaPessoa.setIdade(umaIdade);

                    System.out.println("Digite o email:");
                    entradaTeclado = leitorEntrada.readLine();
                    String umEmail = entradaTeclado;
                    umaPessoa.setEmail(umEmail);

                    System.out.println("Digite o Endereco:");
                    entradaTeclado = leitorEntrada.readLine();
                    String umEndereco = entradaTeclado;
                    umaPessoa.setEndereco(umEndereco);

                    System.out.println("Digite o RG:");
                    entradaTeclado = leitorEntrada.readLine();
                    String umRg = entradaTeclado;
                    umaPessoa.setRg(umRg);

                    System.out.println("Digite o CPF:");
                    entradaTeclado = leitorEntrada.readLine();
                    String umCpf = entradaTeclado;
                    umaPessoa.setCpf(umCpf);
                } else {
                    opcao = 5.0;
                }

            } else if (opcao == 2.0) {
                System.out.print("Digite a pessoa que deseja pesquisar:");
                entradaTeclado = leitorEntrada.readLine();
                String nome = entradaTeclado;
                Pessoa retorno = umControle.pesquisar(nome);
		System.out.println("===============================Contato================================");                
		System.out.println("Nome: " + retorno.getNome());
		System.out.println("Telefone: " + retorno.getTelefone());
		System.out.println("Idade: " + retorno.getIdade());
		System.out.println("Email: " + retorno.getEmail());
		System.out.println("Endereço: " + retorno.getEndereco());
		System.out.println("RG: " + retorno.getRg());
		System.out.println("CPF: " + retorno.getCpf());
		System.out.println("======================================================================");

            } else if (opcao == 3.0) {
                System.out.print("Digite o nome da pessoa a remover:");
                entradaTeclado = leitorEntrada.readLine();
                String removePessoa = entradaTeclado;
                String alerta = umControle.remover(umaPessoa);
                System.out.println(alerta);
            }

        } while (opcao != 0);


    }
}
